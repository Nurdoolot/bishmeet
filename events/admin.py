from django.contrib import admin
from .models import Event, EventMember

class EventMemberInLine(admin.TabularInline):
    model = EventMember


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'group', 'image', 'date']
    inlines = [EventMemberInLine]