from django.shortcuts import get_object_or_404
from celery import shared_task
from time import sleep
from django.core.mail import send_mail
from .models import Event, EventMember

@shared_task
def sleepy(duration):
    sleep(duration)
    return None

@shared_task
def send_email_task(pk):
    event = Event.objects.get(id=pk)
    members = EventMember.objects.filter(event=event)
    for user in members:
        sleep(5)
        send_mail(event.title,
        'Не пропустите мероприятие от группы' + event.title + ' завтра в' + str(event.date),
        'rayfi.vv@gmail.com',
        [user.user.email])

    return None
