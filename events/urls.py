from django.urls import path
from .views import (
    EventListView,
    EventCreateView,
    EventUpdateView,
    EventDetailView,
    EventDeleteView,
    VisitEvent,
    send
)

urlpatterns = [
    path('send/<int:pk>/', send, name='send'),
    path('events/', EventListView.as_view(), name='events'),
    path('event/<int:pk>/', EventDetailView.as_view(), name='event_detail'),
    path('<int:pk>/event/create/', EventCreateView.as_view(), name='event'),
    path('event/update/<int:pk>/', EventUpdateView.as_view(), name='event_update'),
    path('event/delete/<int:pk>/', EventDeleteView.as_view(), name='event_delete'),
    path('visit/', VisitEvent.as_view(), name='visit')
]
