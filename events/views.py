from django.shortcuts import render, redirect, get_object_or_404, HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.base import TemplateResponseMixin, View
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from groups.models import Group
from .models import Event, EventMember
from .forms import EventFormSet, EventForm
from django.http import HttpResponse
from .tasks import send_email_task

def send(request, pk):
    send_email_task.delay(pk)
    return redirect('events')


class EventListView(ListView):
    model = Event
    template_name = 'events/events.html'
    context_object_name = 'events'


class EventDetailView(DetailView):
    model = Event
    template_name = 'events/event.html'
    context_object_name = 'event'

    def get(self, request, *args, **kwargs):
        event = Event.objects.get(id=self.kwargs.get('pk'))
        members = EventMember.objects.filter(event=event)
        return self.render_to_response({'event': event, 'members':members})


class EventCreateView(LoginRequiredMixin, TemplateResponseMixin, View):
    login_url = 'account_login'
    template_name = 'events/create.html'
    group = None

    def dispatch(self, request, pk):
        if request.user.is_authenticated:
            self.group = get_object_or_404(
                Group, id=pk, administration=request.user)
        return super(EventCreateView, self).dispatch(request, pk)

    def get(self, request, *args, **kwargs):
        formset = EventFormSet()
        return self.render_to_response({'group': self.group, 'formset': formset})

    def post(self, request, *args, **kwargs):
        formset = EventFormSet(
            data=request.POST, files=request.FILES, instance=self.group)
        if formset.is_valid():
            formset.save()
            return redirect('detail', self.group.id, self.group.slug)
        return self.render_to_response({'group': self.group, 'formset': formset})


class EventUpdateView(LoginRequiredMixin, TemplateResponseMixin, View):
    login_url = 'account_login'
    template_name = 'events/update.html'
    event = None

    def dispatch(self, request, pk):
        if request.user.is_authenticated:
            self.event = get_object_or_404(
                Event, id=pk, group__administration=request.user)
        return super(EventUpdateView, self).dispatch(request, pk)

    def get(self, request, *args, **kwargs):
        formset = EventForm(instance=self.event)
        return self.render_to_response({'formset': formset})

    def post(self, request, *args, **kwargs):
        formset = EventForm(
            data=request.POST, files=request.FILES, instance=self.event)
        if formset.is_valid():

            formset.save()
            return redirect('event_detail', self.event.id)
        return self.render_to_response({'formset': formset})


class EventDeleteView(LoginRequiredMixin, TemplateResponseMixin, View):
    login_url = 'account_login'
    template_name = 'events/delete.html'
    event = None

    def dispatch(self, request, pk):
        if request.user.is_authenticated:
            self.event = get_object_or_404(
                Event, id=pk, group__administration=request.user)
        return super(EventDeleteView, self).dispatch(request, pk)

    def get(self, request, *args, **kwargs):
        event = get_object_or_404(Event, id=self.kwargs.get('pk'))
        return self.render_to_response({'event': event})

    def post(self, request, *args, **kwargs):
        event = get_object_or_404(Event, id=self.kwargs.get('pk'))
        event.delete()
        return HttpResponseRedirect("/")


class VisitEvent(LoginRequiredMixin, TemplateResponseMixin, View):
    login_url = 'account_login'


    def post(self, request, *args, **kwargs):
        user = request.user
        if request.method == 'POST':
            event_id = request.POST.get('event_id')
            event = Event.objects.get(id=event_id)

            if user in event.members.all():
                event.members.remove(user)
            else:
                event.members.add(user)

            members, created = EventMember.objects.get_or_create(user=user, event_id=event_id)
            print(members, created)
            if not created:
                if members.value == 'attend':
                    members.value = 'not_going'
                else:
                    members.value = 'attend'
            members.save()
        return redirect('event_detail', event.id)
