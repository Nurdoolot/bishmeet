from django import forms
from groups.models import Group
from django.forms.models import inlineformset_factory
from .models import Event

EventFormSet = inlineformset_factory(Group, Event, fields=[
                                     'title', 'description', 'image', 'date', 'place'], 
                                     extra=1, 
                                     can_delete=False,
                                     widgets = {
                                        'date' : forms.DateInput(attrs={'class' : 'date_picker', 'placeholder': '2020-08-13 09:20:00'})
        })

class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = '__all__'
        exclude = ('organizer', 'group', 'members')
        widgets = {'date' : forms.DateInput(attrs={'class' : 'date_picker', 'placeholder': '2020-08-13 09:20:00'})}