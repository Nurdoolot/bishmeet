from django.db import models
from django.contrib.auth.models import User
from groups.models import Group


class Event(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()
    image = models.ImageField(upload_to='events/')
    group = models.ForeignKey(Group, on_delete=models.CASCADE, related_name='event')
    place = models.CharField(max_length=255)
    members = models.ManyToManyField(User, default=None, blank=True, related_name='participants')
    date = models.DateTimeField(
        auto_now=False, auto_now_add=False, blank=True, null=True)
        

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('event_detail', args=[self.id])


CHOICES = (
    ('attend', 'attend'),
    ('not_going', 'not_going')
)

class EventMember(models.Model):
    event = models.ForeignKey(
        Event, related_name='event', on_delete=models.CASCADE)
    user = models.ForeignKey(
        User, on_delete=models.CASCADE)
    value = models.CharField(choices=CHOICES, default='attend', max_length=10)



    def __str__(self):
        return self.user.username


    def get_absolute_url(self):
        return reverse('event_detail', args=[self.id])


