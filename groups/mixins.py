from .models import Group
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy


class AdminMixin(object):
    def get_queryset(self):
        qs = super(AdminMixin, self).get_queryset()
        return qs.filter(administration=self.request.user)


class AdminEditMixin(object):
    def form_valid(self, form):
        form.instance.administration = self.request.user
        return super(AdminEditMixin, self).form_valid(form)


class AdminGroupMixin(AdminMixin, LoginRequiredMixin):
    model = Group
    login_url = 'account_login'


class AdminGroupEditMixin(AdminGroupMixin, AdminEditMixin):
    fields = ['title', 'description', 'category', 'image']
    success_url = reverse_lazy('groups')
    template_name = 'groups/create.html'
