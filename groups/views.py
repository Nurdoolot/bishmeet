from django.shortcuts import render, get_object_or_404, redirect, HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import RedirectView
from django.contrib import messages
from django.db import IntegrityError
from django.urls import reverse, reverse_lazy
from django.views.generic.base import TemplateResponseMixin, View
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from bishmeet.utils import gen_slug, transliterate
from events.models import Event
from .models import Group, Category, Discussion, GroupMember, GroupImage, Administration
from .forms import GroupForm, GroupUpdateForm, DiscussionForm
from .mixins import AdminGroupEditMixin, AdminGroupMixin, AdminEditMixin


class HomeListView(ListView):
    template_name = 'groups/index.html'
    model = Group
    def get_context_data(self):
        categories = Category.objects.all()
        groups = Group.objects.order_by('-created',)[:7]
        events = Event.objects.order_by('-id',)[:7]
        context_object_name = {'categories': categories, 'groups': groups, 'events': events}
        return context_object_name


class GroupsListView(ListView):
    template_name = 'groups/groups.html'
    context_object_name = 'groups'

    def get_queryset(self):
        category = None
        groups = Group.objects.all()
        if self.kwargs.get('slug'):
            category = get_object_or_404(
                Category, slug=self.kwargs.get('slug'))
            return Group.objects.filter(category=category)
        return groups


class GroupDetailView(DetailView):
    template_name = 'groups/detail.html'

    def get(self, request, *args, **kwargs):
        group = Group.objects.get(id=self.kwargs.get('pk'))
        event = Event.objects.filter(group=group)
        members = GroupMember.objects.filter(group=group)
        return self.render_to_response({'event': event, 'group':group, 'members': members})


class GroupCreateView(AdminGroupEditMixin, CreateView):
    template_name = 'groups/create.html'

    def post(self, request, *args, **kwargs):
        if request.method == 'POST':
            form = GroupForm(request.POST, request.FILES)
            if form.is_valid():
                group_form = form.save(commit=False)
                group_form.admin = self.request.user
                group_form.slug = transliterate(gen_slug(group_form.title))
                group_form.save()
                group_form.administration.add(self.request.user)
                return redirect('/')
        else:
            form = GroupUpdateForm()


class GroupUpdateView(AdminGroupEditMixin, UpdateView):
    template_name = 'groups/update.html'
    

    def post(self, request, *args, **kwargs):
        group = Group.objects.get(id=self.kwargs.get('pk'))
        form = GroupUpdateForm(instance=group)
        if request.method == 'POST':
            form = GroupUpdateForm(request.POST, request.FILES, instance=group)
            if form.is_valid():
                group_form = form.save(commit=False)
                group_form.slug = transliterate(gen_slug(group_form.title))
                group_form.save()
                return redirect('/')
        else:
            form = GroupUpdateForm()


class GroupDeleteView(AdminGroupMixin, DeleteView):
    template_name = 'groups/delete.html'
    success_url = reverse_lazy('groups')

class JoinGroup(TemplateResponseMixin, View):
    login_url = 'account_login'


    def post(self, request, *args, **kwargs):
        user = request.user
        if request.method == 'POST':
            group_id = request.POST.get('group_id')
            group = Group.objects.get(id=group_id)

            if user in group.members.all():
                group.members.remove(user)
            else:
                group.members.add(user)

            members, created = GroupMember.objects.get_or_create(user=user, group_id=group_id)
            print(members, created)
            if not created:
                if members.value == 'join':
                    members.value = 'leave'
                else:
                    members.value = 'join'
            members.save()
            if members.value == 'leave':
                members.delete()
            
        return redirect('detail', group.id, group.slug)



class SharePer(AdminGroupEditMixin, TemplateResponseMixin, View):
    template_name = 'groups/members.html'
    login_url = 'account_login'


    def get(self, request, *args, **kwargs):
        group = Group.objects.get(id=self.kwargs.get('pk'))
        members = GroupMember.objects.filter(group=group)
        return self.render_to_response({'members': members, 'group': group})
            
    
    def post(self, request, *args, **kwargs):
        user_id = request.POST.get('user_id')
        user = GroupMember.objects.get(id=user_id)
        print(user)
        if request.method == 'POST':
            
            group_id = request.POST.get('group_id')
            group = Group.objects.get(id=group_id)
            if user.user in group.administration.all():
                group.administration.remove(user.user)
            else:
                group.administration.add(user.user)
                print("ok")
            administration, created = Administration.objects.get_or_create(user=user.user, group_id=group_id)
            print(administration, created)
            if not created:
                if administration.value == 'add':
                    administration.value = 'remove'
                else:
                    administration.value = 'add'
            administration.save()
            if administration.value == 'remove':
                administration.delete()
            
        return HttpResponseRedirect('/')