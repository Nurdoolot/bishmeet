from django import forms
from .models import Group, Discussion
from django.forms.models import inlineformset_factory


class GroupForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = ('title', 'description', 'category', 'image')


class GroupUpdateForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = ('title', 'description', 'image')


class DiscussionForm(forms.ModelForm):
    class Meta:
        model = Discussion
        fields = ('text',)
