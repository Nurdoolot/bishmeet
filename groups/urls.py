from django.urls import path
from .views import (HomeListView,
                    GroupsListView,
                    GroupDetailView,
                    GroupCreateView,
                    GroupUpdateView,
                    GroupDeleteView,
                    JoinGroup,
                    SharePer
                    )


urlpatterns = [
    path('', HomeListView.as_view(), name='home'),
    path('groups/', GroupsListView.as_view(), name='groups'),
    path('create/', GroupCreateView.as_view(), name='create'),
    path('<int:pk>/<slug:slug>/', GroupDetailView.as_view(), name='detail'),
    path('update/<int:pk>/', GroupUpdateView.as_view(), name='update'),
    path('delete/<int:pk>/', GroupDeleteView.as_view(), name='delete'),
    path('join/', JoinGroup.as_view(), name='join'),
    path('members/<int:pk>/', SharePer.as_view(), name='members'),
    path('share/per/', SharePer.as_view(), name='share'),
    path('groups/<slug:slug>/', GroupsListView.as_view(), name='group_by_category'),
]
