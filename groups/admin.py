from django.contrib import admin
from .models import Group, Category, GroupMember, GroupImage, Administration


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['title', 'slug', 'image']
    prepopulated_fields = {'slug': ('title',)}


class GroupMemberInLine(admin.TabularInline):
    model = GroupMember


class GroupImageInLine(admin.TabularInline):
    model = GroupImage

class Administration(admin.TabularInline):
    model = Administration

@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    list_display = ['title', 'slug', 'category', 'image', 'created', 'updated']
    prepopulated_fields = {'slug': ('title',)}
    inlines = [Administration, GroupMemberInLine, GroupImageInLine]
