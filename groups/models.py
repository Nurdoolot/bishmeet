from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
from bishmeet.utils import gen_slug, transliterate


class Category(models.Model):
    title = models.CharField(max_length=150)
    image = models.ImageField(upload_to='categories/')
    slug = models.SlugField(unique=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = transliterate(gen_slug(self.title))
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('group_by_category', args=[self.slug])


class Group(models.Model):
    title = models.CharField(max_length=255, db_index=True)
    slug = models.SlugField(max_length=200, db_index=True)
    admin = models.ForeignKey(User, on_delete=models.CASCADE)
    description = models.TextField()
    category = models.ForeignKey(
        Category, related_name='groups', on_delete=models.DO_NOTHING)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    image = models.ImageField(upload_to='group/', blank=True)
    members = models.ManyToManyField(
        User, default=None, blank=True, related_name='members')
    administration = models.ManyToManyField(
        User, default=None, blank=True, related_name='administration')

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('title',)
        index_together = (('id', 'slug'))

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = transliterate(gen_slug(self.title))

        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('detail', args=[self.id, self.slug])


CHOICES = (
    ('join', 'join'),
    ('leave', 'leave')
)


class GroupMember(models.Model):
    group = models.ForeignKey(
        Group, related_name='memberships', on_delete=models.CASCADE)
    user = models.ForeignKey(
        User, related_name='user_groups', on_delete=models.CASCADE, db_index=True)
    value = models.CharField(choices=CHOICES, default='join', max_length=10)

    def __str__(self):
        return self.user.username

    class Meta:
        unique_together = ('group', 'user')

    def get_absolute_url(self):
        return reverse('detail', args=[self.id, Group.slug])


SHARE_CHOICES = (
    ('add', 'add'),
    ('remove', 'remove')
)


class Administration(models.Model):
    user = models.ForeignKey(User, related_name='user',
                             on_delete=models.CASCADE, db_index=True)
    group = models.ForeignKey(
        Group, related_name='group', on_delete=models.CASCADE)
    value = models.CharField(choices=SHARE_CHOICES,
                             default='add', max_length=10)

    def __str__(self):
        return str(self.group)


class GroupImage(models.Model):
    group = models.ForeignKey(Group, default=None, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='images/', blank=True, null=True)

    def __str__(self):
        return self.group.title


class Discussion(models.Model):
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.TextField()

    def __str__(self):
        return f'{self.group.title}, {self.user}'
