from django.db import models
from django.contrib.auth.models import User


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=155, null=True, blank=True)
    last_name = models.CharField(max_length=155, null=True, blank=True)
    email = models.EmailField()
    photo = models.ImageField(upload_to='users/')
    age = models.PositiveIntegerField(default=18, blank=True)
    number = models.PositiveIntegerField(blank=True, null=True)


    def __str__(self):
        return self.first_name