# Generated by Django 3.0.5 on 2020-06-14 17:12

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(blank=True, max_length=155, null=True)),
                ('last_name', models.CharField(blank=True, max_length=155, null=True)),
                ('email', models.EmailField(max_length=254)),
                ('photo', models.ImageField(upload_to='users/')),
                ('age', models.PositiveIntegerField(blank=True, default=18)),
                ('number', models.PositiveIntegerField(blank=True, null=True)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
