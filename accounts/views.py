from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from .form import ProfileForm
from groups.models import Group
from events.models import Event


@login_required(login_url='login')
def profile(request):
    profile = request.user.profile
    form = ProfileForm(instance=profile)
    if request.method == 'POST':
        form = ProfileForm(request.POST, request.FILES, instance=profile)
        if form.is_valid():
            form.save()
    context = {'form': form}
    return render(request, 'account/profile.html', context)







@login_required(login_url='login')
def user_page(request):
    user = request.user
    groups = Group.objects.filter(members=user)
    events = Event.objects.filter(members=user)
    recommendations = Event.objects.order_by('?')[:7]
    context = {'groups': groups, 'events': events, 'user': user, 'recommendations': recommendations}
    return render(request, 'account/user_page.html', context)

@login_required(login_url='login')
def notifications(request):
    user = request.user
    events = Event.objects.filter(members=user).order_by('-date')
    context = {'events': events}
    return render(request, 'account/notifications.html', context)


@login_required(login_url='login')
def calendar(request):
    user = request.user
    events = Event.objects.filter(members=user).order_by('date')
    context = {'events': events}
    return render(request, 'account/calendar.html', context)
