from django.urls import path, include
from .views import profile, user_page, notifications, calendar

urlpatterns = [
    path('', include('allauth.urls')),
    path('profile/', profile, name='profile'),
    path('user_page/', user_page, name='user_page'),
    path('notifications/', notifications, name='notifications'),
    path('calendar/', calendar, name='calendar')
]